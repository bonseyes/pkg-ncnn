/**
* NVISO CONFIDENTIAL
*
* Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
*
* The source code contained or described herein and all documents related to
* the source code ("Material") is the confidential and proprietary information
* owned by nViso or its suppliers or licensors.  Title to the  Material remains
* with nViso Sarl or its suppliers and licensors. The Material contains trade
* secrets and proprietary and confidential information of nViso or its suppliers
* and licensors. The Material is protected by worldwide copyright and trade
* secret laws and treaty provisions. You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the license
* agreement you entered into with nViso.
*
* NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

#include "FaceDetectorSsdNcnn.hpp"

#include "nviso_utils.hpp"

#include "nvisocv/imgproc.hpp"
#include "ModelConfigs.hpp"

// ncnn
#include "net.h"

#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

namespace nviso {

struct Object {
  cv::Rect_<float> rect;
  int label;
  float prob;
};


struct FaceDetectorSsdNcnn::Private {
  Private() {}

  map<string, unique_ptr<ncnn::Net>> nets_;

  /// Current model
  ncnn::Net* net_{};

  string model_;
  ModelConfigs modelConfigs_;
  float threshold_ = 0.7f;
  float maxOverlap_ = 0.0f;  ///< Non_max_suppression threshold (0 disable)
  int threadNumber_ = 1;
};

FaceDetectorSsdNcnn::FaceDetectorSsdNcnn() : d(new Private) {}

FaceDetectorSsdNcnn::~FaceDetectorSsdNcnn() { delete d; }

class Noop : public ncnn::Layer {};
DEFINE_LAYER_CREATOR(Noop)

bool FaceDetectorSsdNcnn::init(const json& cfg) {
  return d->modelConfigs_.init(cfg);
}

bool FaceDetectorSsdNcnn::setOptions(const json& cfg) {
  setValue(d->threshold_, cfg, "threshold");
  setValue(d->maxOverlap_, cfg, "max_overlap");
  setValue(d->threadNumber_, cfg, "thread_number");

  if (!d->modelConfigs_.setOptions(cfg)) {
    LOGE << "Set options failed";
    return false;
  }

  auto config = d->modelConfigs_.getConfig();

  if (!config || config->weights.empty()) {
      LOGV << "No model";
      return true;
  }

  string model = config->weights;

  auto nit = d->nets_.find(model);
  if (nit != d->nets_.end()) {
    LOGV << "model already loaded " << model;
    d->net_ = nit->second.get();
    return true;
  }

  LOGV << "Loading new model: " << model;
  // d->nets_[model].reset(new ncnn::Net);
  nit = d->nets_.insert(
              make_pair(model, unique_ptr<ncnn::Net>(new ncnn::Net))).first;

  auto net = nit->second.get();
  auto res = net->register_custom_layer("Silence", Noop_layer_creator);
  if (res != 0) {
    LOGE << "Failed to register_custom_layer: " << res;
    return false;
  }

  LOGI << "Loading param " << config->params.c_str();
  res = net->load_param(config->params.c_str());
  if (res != 0) {
    LOGE << "Failed to load model parameters: " << res;
    return false;
  }
  res = net->load_model(config->weights.c_str());
  if (res != 0) {
    LOGE << "Failed to load model weights: " << res;
    return false;
  }
  d->net_ = net;
  return true;
}

bool FaceDetectorSsdNcnn::predict(const ncv::Image& input,
                                  std::list<DetectedFace>* detectedFaces) {
  auto config = d->modelConfigs_.getConfig();

  if (!config || config->weights.empty()) {
    LOGE << "Model is not configured";
    return false;
  }
  if (!d->net_) {
    LOGE << "Model is not initialised";
    return false;
  }
  detectedFaces->clear();
  if (input.channels() != 3 || input.getImageType() != ncv::ImageType::RGB) {
    LOGE << "We need RGB image input";
    return false;
  }

  int imageHeight = input.rows();
  int imageWidth = input.cols();

  Timer t;
  t.tic();
  ncnn::Mat in = ncnn::Mat::from_pixels_resize(
      input.data(), ncnn::Mat::PIXEL_RGB, imageWidth, imageHeight,
      config->input.width, config->input.height);
  LOGV << "resize mat: " << t.toc();

  t.tic();
  const float* mean =
      (config->input.mean[0] <= 0) ? nullptr : &config->input.mean[0];
  const float* norm =
      (config->input.norm[0] <= 0) ? nullptr : &config->input.norm[0];

  in.substract_mean_normalize(mean, norm);
  LOGV << "normalize mat: " << t.toc();

  ncnn::Extractor ex = d->net_->create_extractor();
  ex.set_num_threads(d->threadNumber_);

  auto res = ex.input("data", in);
  if (res != 0) {
    LOGE << "Failed to set input: " << res;
    return false;
  }

  t.tic();
  ncnn::Mat out;
  res = ex.extract("detection_out", out);
  LOGV << "extract: " << t.toc();
  t.tic();

  if (res != 0) {
    LOGE << "Failed to extract result: " << res;
    LOGE << "out " << out;
    LOGE << "out total() " << out.total() << "out w " << out.w << "out h "
         << out.h << "out c " << out.c;
    return false;
  }

  std::vector<DetectedFace> faces;

  struct NcnnSsdResult {
    // ncnn does not give sampleIx
    // float sampleIx;
    float labelId;
    float confidence;
    float x1;
    float y1;
    float x2;
    float y2;
  };

  for (int i = 0; i < out.h; i++) {
    auto box = reinterpret_cast<const NcnnSsdResult*>(out.row(i));

    // Skip detection if confidence is too low
    if (box->confidence < d->threshold_) continue;

    // Top left corner
    float x1 = box->x1 * imageWidth;
    float y1 = box->y1 * imageHeight;
    // Bottom right corner
    float x2 = box->x2 * imageWidth;
    float y2 = box->y2 * imageHeight;

    DetectedFace face = {
        box->confidence, {x1, y1, x2 - x1, y2 - y1}, Landmarks{}};

    detectedFaces->push_back(face);
  }

  return true;
}
}
