#pragma once
/**
* NVISO CONFIDENTIAL
*
* Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
*
* The source code contained or described herein and all documents related to
* the source code ("Material") is the confidential and proprietary information
* owned by nViso or its suppliers or licensors.  Title to the  Material remains
* with nViso Sarl or its suppliers and licensors. The Material contains trade
* secrets and proprietary and confidential information of nViso or its suppliers
* and licensors. The Material is protected by worldwide copyright and trade
* secret laws and treaty provisions. You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the license
* agreement you entered into with nViso.
*
* NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

#include "FaceDetector.hpp"

namespace nviso {

/// Algorithm to detect identify in an image using LPDNN
class FaceDetectorSsdNcnn : public FaceDetector {
 public:
  FaceDetectorSsdNcnn();
  ~FaceDetectorSsdNcnn() override;

  bool init(const json& cfg) override;
  bool setOptions(const json& cfg) override;
  void setMinMax(int, int) override {}
  bool predict(const ncv::Image& input,
               std::list<DetectedFace>* faces) override;

 private:
  struct Private;
  Private* d;
};
}
