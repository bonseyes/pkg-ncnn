/**
* NVISO CONFIDENTIAL
*
* Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
*
* The source code contained or described herein and all documents related to
* the source code ("Material") is the confidential and proprietary information
* owned by nViso or its suppliers or licensors.  Title to the  Material remains
* with nViso Sarl or its suppliers and licensors. The Material contains trade
* secrets and proprietary and confidential information of nViso or its suppliers
* and licensors. The Material is protected by worldwide copyright and trade
* secret laws and treaty provisions. You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the license
* agreement you entered into with nViso.
*
* NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

#include "RecognitionProcessorNcnn.hpp"

#include <cstring>
#include <iostream>
#include <string>

using namespace std;

namespace nviso {

struct RecognitionProcessorNcnn::Private {
};

RecognitionProcessorNcnn::RecognitionProcessorNcnn() : d(new Private) {}

RecognitionProcessorNcnn::~RecognitionProcessorNcnn() { delete d; }

bool RecognitionProcessorNcnn::init(const json& cfg) {
  bool res = false; //d->init(cfg);
  return res;
}

bool RecognitionProcessorNcnn::setOptions(const json& cfg) {
  return false; //d->setOptions(cfg);
}

bool RecognitionProcessorNcnn::predict(const ncv::ImageTile& image,
                                        FaceDescriptor* recognition) {
  // Perform inference
  size_t resultLen;
  const float* result = nullptr; //d->predict(image, &resultLen);
  if (result == nullptr) {
    return false;
  }

  // Copy result
  recognition->resize(resultLen);
  memcpy(&(*recognition)[0], result, sizeof(float) * resultLen);

  return true;
}
}
