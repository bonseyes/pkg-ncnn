#pragma once
/**
* NVISO CONFIDENTIAL
*
* Copyright (c) 2009- 2014 nViso SA. All Rights Reserved.
*
* The source code contained or described herein and all documents related to
* the source code ("Material") is the confidential and proprietary information
* owned by nViso or its suppliers or licensors.  Title to the  Material remains
* with nViso Sarl or its suppliers and licensors. The Material contains trade
* secrets and proprietary and confidential information of nViso or its suppliers
* and licensors. The Material is protected by worldwide copyright and trade
* secret laws and treaty provisions. You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the license
* agreement you entered into with nViso.
*
* NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
* THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
* TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
* ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
* DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/

#include "RecognitionProcessor.hpp"

namespace nviso {

/// Algorithm to detect identify in an image using LPDNN
class RecognitionProcessorNcnn : public RecognitionProcessor {
 public:
  RecognitionProcessorNcnn();
  ~RecognitionProcessorNcnn() override;

  /// Init network
  bool init(const json& cfg) override;

  /// Configure options
  bool setOptions(const json& cfg) override;

  /// Predict
  bool predict(const ncv::ImageTile& image,
               FaceDescriptor* recognition) override;

 private:
  struct Private;
  Private* d;
};
}
