macro(caffe_to_ncnn in outdir outname quantization targetname)

  set(in_param ${in}.prototxt)
  set(in_bin ${in}.caffeweights)
  set(out_param ${outdir}/${outname}.param)
  set(out_bin ${outdir}/${outname}.bin)
  set(out_id ${outdir}/${outname}.id.h)
  set(out_mem ${outdir}/${outname}.mem.h)
  set(out_param_bin ${out_param}.bin)

  add_custom_target(${targetname}
    DEPENDS ${in_param} ${in_bin}
    BYPRODUCTS ${out_param} ${out_bin} ${out_id} ${out_mem} ${out_param_bin}
    COMMAND ${CMAKE_COMMAND} -E make_directory ${outdir}
    COMMAND ${caffe2ncnn_bin} ${in_param} ${in_bin} ${out_param} ${out_bin} ${quantization}
    #COMMAND ${ncnn2mem_bin} ${out_param} ${out_bin} ${out_id} ${out_mem}
    )
  add_dependencies(${targetname} ncnn_tools)
  add_dependencies(ncnn_models ${targetname})
endmacro()
